# Agile Frameworks

![ScrumDevOps](img/the-agile-scrum-framework.png)

* Scrum
    * sprint
    * User Story
    * Acceptance Criteria
    * Definition of Ready / Done

* Kanban
    * board
    * JIT
    * limit 

# Scale out

![SAFe](img/safe.png)

* [SAFe](https://www.scaledagileframework.com/)
* [less](https://less.works/)

# Work Item Workflow

* Created
* Ready
* Implemented
* Done

# Tooling
* TFS
* [Jira](https://www.atlassian.com/software/jira)

